package com.gyungdal.Header;

import java.sql.Connection;

/**
 * Created by GyungDal on 2016-07-14.
 */

public class Config {

    //Macro Variable
    public static final int MAX_CLNT = 1024;		//접속 가능한 최대 유저 수
    public static final int MAX_ROOM = 256;			//생성 가능한 방 최대 수
    public static final int BUF_SIZE = 1024;
    public static final int TIME_LIMIT = 30;
    public static final int SERV_PORT = 6733;

    //MySQL 접속을 위한 인자 값들
    public static final String DBHOST = null;			//DB 호스트
    public static final String DBUSER = "root";		//DB 유저
    public static final String DBPASSWD = "password";	//DB 유저 패스워드
    public static final String DBNAME = "scv";			//DB 이름
    public static final int DBPORT = 0;				//DB 포트
    public static final Config DBSOCKNAME = null;		//DB 소켓 이름
    public static Connection con;
}
