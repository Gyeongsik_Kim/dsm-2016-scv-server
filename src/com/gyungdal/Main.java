package com.gyungdal;
import com.gyungdal.Header.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;




public class Main {
    public static void main(String[] args) {
        try {
            Connection con = null;
            con = DriverManager.getConnection(Config.DBHOST,
                    Config.DBUSER, Config.DBPASSWD);
            Config.con = con;
        }catch(SQLException e){
            System.out.println(e.getCause());
            return;
        }
    }
}
